package net.iescierva.ramonmr95.p0303calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private TextView resultTextView;
    private String operator;
    private boolean isOperation;
    private boolean isDigitoPulsado;
    private boolean isIgualPulsado;
    private boolean isRepetidoIgual;
    private double operand1;
    private double operand2;
    private Locale locale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultTextView = findViewById(R.id.resultTextView);
        init();
    }

    public void createNumber(View view) {
        Button btn = (Button)view;
        String text = btn.getText().toString();
        appendNumber(text);
    }

    public void appendNumber(String number) {
        this.resultTextView.setTextColor(Color.BLACK);

        if (isIgualPulsado) {
            operand1 = 0;
            operand2 = 0;
            isDigitoPulsado = false;
            resultTextView.setText("0");
            operator = "1º";
        }
        else if (isOperation) {
            resultTextView.setText("");
        }

        if (resultTextView.getText().toString().equals("0")) {
            resultTextView.setText(number);
        }
        else {
            String res = resultTextView.getText().toString() + number;
            resultTextView.setText(String.format(locale, "%s", res));
        }
        isOperation = false;
        isDigitoPulsado = true;
        isIgualPulsado = false;
    }

    public void addOperator(View view) {
        if (isDigitoPulsado || isIgualPulsado) {
            switch (view.getId()) {
                case R.id.sumButton:
                    operation("+");
                    break;
                case R.id.subButton:
                    operation("-");
                    break;
                case R.id.divButton:
                    operation("/");
                    break;
                case R.id.prodButton:
                    operation("*");
                    break;
                case R.id.modButton:
                    operation("%");
                    break;
            }
            this.isOperation = true;
        }
        else {
            Button btn = (Button)view;
            operator = btn.getText().toString();
        }

    }

    public void addComma(View view) {
        if (resultTextView.getText().toString().equals("0")) {
            resultTextView.setText("0.");
        }
        else if (!resultTextView.getText().toString().contains(".")) {
            String res = resultTextView.getText() + ".";
            resultTextView.setText(res);
        }
    }

    public void operation(String operation) {
        if (!operation.equals("=")) {
            this.operand2 = Double.parseDouble(resultTextView.getText().toString());
        }

        if (isDigitoPulsado || operation.equals("=")) {
            switch (operator) {
                case "1º":
                    operand1 = operand2;
                    break;
                case "+":
                    operand1 += operand2;
                    break;
                case "-":
                    operand1 -= operand2;
                    break;
                case "/":
                    if (operand2 != 0) {
                        operand1 /= operand2;
                    }
                    else {
                        resetAC(null);
                        this.resultTextView.setText(R.string.err_text);
                        this.resultTextView.setTextColor(Color.RED);
                        return;
                    }
                    break;
                case "*":
                   operand1 *= operand2;
                    break;
                case "%":
                    operand1 %= operand2;
                    break;
            }
        }

        if (!operator.equals("1º")) {
            this.resultTextView.setText(String.valueOf(operand1));
        }

        if (operation.equals("=")) {
            isIgualPulsado = true;
        }
        else {
            operator = operation;
            isIgualPulsado = false;
        }
        isDigitoPulsado = false;
    }

    public void equals(View view) {
        if (isIgualPulsado) {
            operation("=");
            isRepetidoIgual = true;
        }
        else if (isDigitoPulsado || isRepetidoIgual) {
            operation(operator);
        }
        isIgualPulsado = true;
        isOperation = false;
        isDigitoPulsado = false;
    }

    public void resetAC(View view) {
        reset(null);
        isIgualPulsado = false;
        isOperation = false;
        isDigitoPulsado = true;
        isRepetidoIgual = false;
    }

    public void reset(View view) {
        this.resultTextView.setTextColor(Color.BLACK);
        this.operand2 = 0;
        this.operand1 = 0;
        this.isOperation = false;
        this.resultTextView.setText("0");
        operator = "1º";
    }

    public void cleanResult(View view) {
        this.resultTextView.setTextColor(Color.BLACK);
        resultTextView.setText("0");
    }

    public void init() {
        operand1 = 0;
        operand2 = 0;
        operator = "1º";
        isDigitoPulsado = true;
        resultTextView.setText("0");
        locale = Locale.getDefault();
    }

}
